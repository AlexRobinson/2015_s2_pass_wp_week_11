/**
 * Created by AlexRob on 5/10/15.
 */

/**
 * Chat Constructor
 *
 * @constructor
 */
var Chat = function (name) {

  this.name = name;
  this.onUpdate = null;

};

Chat.prototype.sendMessage = function (message) {
  $.ajax({
    url: '../app/api/chat.php',
    method: 'post',
    data: {
      name: this.name,
      message: message
    }
  }).done(function (response) {
    response = JSON.parse(response);

    if (response.success) {
      if (this.onUpdate) {
        this.onUpdate(response.messages);
      }
    } else {
      console.error('Something went wrong');
      console.error(response);
    }
  }.bind(this))
};

Chat.prototype.deleteAll = function () {
  this.onUpdate([]);
  $.ajax({
    url: '../app/api/deleteall.php',
    method: 'get'
  }).done(function (response) {
    /* Do nothing */
  });
};

Chat.prototype.createMessageHTML = function (name, message) {
  return ("<div class='Message'>" +
  "<h3>" + name + "</h3>" +
  "<p>" + message + "<p>" +
  "</div>");
}