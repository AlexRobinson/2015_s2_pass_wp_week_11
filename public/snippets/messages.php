<?php
/**
 * Created by PhpStorm.
 * User: AlexRob
 * Date: 5/10/15
 * Time: 10:33 PM
 */


require_once(__DIR__ . '/../../app/service/chat.php');

$messages = getChatData()->messages;

foreach($messages as $message) {
    $name = $message->name;
    $message = $message->message;
    include (__DIR__ . '/../templates/message.php');
}

?>