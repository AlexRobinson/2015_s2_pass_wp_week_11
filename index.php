<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title></title>

    <link rel="stylesheet" href="public/styles/main.css" />
</head>
<body>

<div class="Chat">

    <div class="Chat_Window MessageList" id="messageList">
        <?php include('public/snippets/messages.php'); ?>
    </div>
    <div class="Chat_Input">
        <input type="text" id="inputText" /> <button id="inputButton" class="Button Button--success">Send</button>
    </div>
    <div style="text-align: right">
        <button id="deleteAllButton" class="Button Button--danger">Delete All</button>
    </div>
</div>

<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="public/scripts/chat.js"></script>
<script>
    var chatSystem = new Chat('PASS');

    var inputText = document.getElementById('inputText');
    var inputButton = document.getElementById('inputButton');
    var deleteAllButton = document.getElementById('deleteAllButton');
    var messageList = document.getElementById('messageList');
    messageList.scrollTop = messageList.scrollHeight;

    inputButton.addEventListener('click', function () {
        chatSystem.sendMessage(inputText.value);
        inputText.value = '';
    });

    deleteAllButton.addEventListener('click', function () {
        chatSystem.deleteAll();
    });

    chatSystem.onUpdate = function (messages) {
        var newMessages = '';
        for(var i = 0; i < messages.length; i++) {
            newMessages += chatSystem.createMessageHTML(messages[i].name, messages[i].message);
        }
        messageList.innerHTML = newMessages;
        messageList.scrollTop = messageList.scrollHeight;
    }

</script>
</body>
</html>