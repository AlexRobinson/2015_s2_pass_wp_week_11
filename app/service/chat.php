<?php
/**
 * Created by PhpStorm.
 * User: AlexRob
 * Date: 5/10/15
 * Time: 10:24 PM
 */

const CHAT_FILE = __DIR__ .'/../../chat.json';

function getChatData() {
    $chatRaw = file_get_contents(CHAT_FILE);
    $chatJson = json_decode($chatRaw);
    return $chatJson;
}

function addMessage($name, $message) {
    $chatData = getChatData();
    array_push($chatData->messages, [
        'name' => $name,
        'message' => $message
    ]);
    file_put_contents(CHAT_FILE, json_encode($chatData), LOCK_EX);
    return $chatData;
}

function deleteAllMessages() {
    $chatData = getChatData();
    $chatData->messages = [];
    file_put_contents(CHAT_FILE, json_encode($chatData), LOCK_EX);
    return $chatData;
}



?>