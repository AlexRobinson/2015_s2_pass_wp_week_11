<?php
/**
 * Created by PhpStorm.
 * User: AlexRob
 * Date: 5/10/15
 * Time: 10:24 PM
 */

require (__DIR__ . '/../service/chat.php');

$errors = array();
$export = array();

if(!isset($_POST['name'])) {
    array_push($errors, 'Missing name');
}

if(!isset($_POST['message'])) {
    array_push($errors, 'Missing message');
}

if(count($errors)) {
    $export['success'] = false;
    $export['errors'] = $errors;
} else {
    $name = $_POST['name'];
    $message = $_POST['message'];

    $newMessages = addMessage($name, $message);
    $export['success'] = true;
    $export['messages'] = $newMessages->messages;
}

echo json_encode($export);

?>